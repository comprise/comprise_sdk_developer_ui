import { Component } from '@angular/core';
import { Router } from '@angular/router';
import * as os from 'os';

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ProjectDataService, ElectronService } from '../core/services';
import { shell } from 'electron';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent {
	public isWindows = true;

	constructor(private router: Router,
		public projectDataService: ProjectDataService,
		public electronService: ElectronService, 
		public httpClient: HttpClient) {
		console.log(this.router.url)

		if (!os.platform().includes('win32')) {
			this.isWindows = false;
		}
		//this.projectDataService.setProjectName("ng-demo");
		//this.projectDataService.setProjectPath("C:\\Users\\Gerrit\\Desktop\\Crossplatform-Projects\\Ionic/ng-demo");
	}

	chooseProjectPath() {
		let env = this;
		this.electronService.dialog.showOpenDialog(this.electronService.remote.getCurrentWindow(), {
			title: "Choose package.json of Angular Project to add COMPRISE SDK Client Library",
			buttonLabel: "Choose Angular Project",
			defaultPath: "C:\\Crossplatform-Projects\\Ionic",
			filters: [{ name: 'Angular project / package.json', extensions: ['json'] }],
			properties: ['openFile']
		}).then(result => {
			var path = result.filePaths[0];
			if (path && path.indexOf("package.json") > -1) {
				//remove file to have folder path + backslashes removed in favour to slashes
				const pathSeparator = this.isWindows ? '\\' : '/';
				var packageFile = JSON.parse(this.electronService.fs.readFileSync(path, "utf8"));
				var name = packageFile.name;
				var description = packageFile.description || "";
				path = path.substring(0, path.lastIndexOf(pathSeparator)).replace(/\\/g, "/");
				console.log(name, path)
				this.projectDataService.setProjectName(name);
				this.projectDataService.setProjectPath(path);
				this.projectDataService.setProjectDescription(description);
				
				var cpFile = JSON.parse(this.electronService.fs.readFileSync(this.projectDataService.projectPath+"/cp.json", "utf8"));
				
			} else if (path && path.indexOf("package.json") == -1) {
				this.electronService.dialog.showMessageBoxSync(this.electronService.remote.getCurrentWindow(), {
					type: "error",
					title: "Wrong file chosen!",
					message: "You have not chosen a package.json-File. Please try again."
				})
			}
		}).catch(err => {
			//console.log(err)
		})
	}
	
	loadModelTypes(server_url) {
		
		if(this.projectDataService.personalizedServerURL.indexOf("https://") == -1) this.projectDataService.setPersonalizedServerURL("https://"+this.projectDataService.personalizedServerURL);
		if(this.projectDataService.personalizedServerURL.charAt(this.projectDataService.personalizedServerURL.length - 1) === "/") {
			this.projectDataService.setPersonalizedServerURL(this.projectDataService.personalizedServerURL.slice(0, -1));	
		}
		
		let headers = new HttpHeaders(); 
		headers = headers.set("Content-Type", "application/json");
		
		this.httpClient.get(this.projectDataService.personalizedServerURL+"/available_stt_files", {headers: headers}).subscribe((models: any)=>{
			var sttModels = [];
			
			for(var model of models) {
				if( model === "vosk-small-en" ) sttModels.push({appid: "5faa8210b7540802f1f2832d", modelid: "vosk-small-en", lang: "en", descr: model});
				if( model === "vosk-small-fr" ) sttModels.push({appid: "5faa8230b7540802f1f2832e", modelid: "vosk-small-fr", lang: "fr", descr: model});
				if( model === "vosk-small-de" ) sttModels.push({appid: "5faa825ab7540802f1f2832f", modelid: "vosk-small-de", lang: "de", descr: model});
				if( model === "latvian-small-stt" ) sttModels.push({appid: "5faa9506b7540802f1f28331", modelid: "6037753040b670c84ae49785", lang: "lv", descr: model});
				if( model === "lithuanian-small-stt" ) sttModels.push({appid: "5faa9539b7540802f1f28332", modelid: "5fc9fbb13a5189220ac99c34", lang: "lt", descr: model});
				if( model === "vosk-small-pt" ) sttModels.push({appid: "5faa828db7540802f1f28330", modelid: "vosk-small-pt", lang: "pt", descr: model});
				if( model === "vosk-large-en" ) sttModels.push({appid: "61701734ddfe91187cf745f7", modelid: "vosk-large-en", lang: "en", descr: model});
				if( model === "vosk-large-fr" ) sttModels.push({appid: "61701340ddfe91187cf745f5", modelid: "vosk-large-fr", lang: "fr", descr: model});
				if( model === "vosk-large-de" ) sttModels.push({appid: "61701376ddfe91187cf745f6", modelid: "vosk-large-de", lang: "de", descr: model});
			}

			
			this.projectDataService.setSttModels(sttModels);
			this.projectDataService.setSelectedSttModels([{appid: "5faa8210b7540802f1f2832d", modelid: "vosk-small-en", lang: "en", descr: "vosk-small-en"}]);			
			console.log("STT_FILES", sttModels)
		})  
	}

	onChangeSTTModel(event) { 
		var inputs = document.getElementsByName("stt_checkboxes");
		var checked = 0;
		var modelsToAdd = [];
		for (var x = 0; x < inputs.length; x++) {
			 
			if((<HTMLInputElement>inputs[x]).checked) {
				checked++;
				var model = this.projectDataService.sttModels.find((m) => { 
					return m.descr === (<HTMLInputElement>inputs[x]).id
				}); 
				
				modelsToAdd.push(model)
			}
		}
		
		console.log("modelsToAdd", modelsToAdd)
		if(checked < 1 && event) { 
			event.preventDefault();
		} else { 
			this.projectDataService.setSelectedSttModels(modelsToAdd);
		}
	}

	navigate(url) {
		shell.openExternal(url);
	}
}
