import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SluComponent } from './slu.component';

const routes: Routes = [
	{
		path: '',
		component: SluComponent
	}
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SluRoutingModule { }
