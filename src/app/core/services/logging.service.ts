import { Injectable, AfterViewInit } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

import { default as AnsiUp } from 'ansi_up';


@Injectable({
  providedIn: 'root'
})
export class LoggingService {
	
	logElem;
	
	isLogged(msg: string) {
		return this.logElem.innerHTML.toString().indexOf(msg) > -1;
	}
	
	hideLog() {
		this.logElem.style.visibility = "collapse";
	}
	
	showLog() {
		this.logElem.style.visibility = "";
	}

	angularLog(msg: string) {
		msg = msg.toString();
		const ansi_up = new AnsiUp();
		let html = ansi_up.ansi_to_html(msg);
		html = html.replace(new RegExp('chunk', 'g'), "<br> chunk");
		var textnode = document.createElement("p"); 
		textnode.className = "consoleText";
		textnode.innerHTML = html;
		this.logElem.appendChild(textnode);    
		this.logElem.scrollTop = this.logElem.scrollHeight; 
	}
	
	info(msg: string): string {
		var textnode = document.createElement("p"); 
		textnode.className = "consoleText";
		textnode.style.color = "white";
		textnode.innerHTML = msg; 
		this.logElem.appendChild(textnode);    
		this.logElem.scrollTop = this.logElem.scrollHeight; 
		return msg;
	}
	
	success(msg: string): string {
		var textnode = document.createElement("p"); 
		textnode.className = "consoleText";
		textnode.style.color = "green";
		textnode.innerHTML = msg; 
		this.logElem.appendChild(textnode);    
		this.logElem.scrollTop = this.logElem.scrollHeight; 
		return msg;
	}
	
	warn(msg: string): string {
		var textnode = document.createElement("p"); 
		textnode.className = "consoleText";
		textnode.style.color = "gold";
		textnode.innerHTML = msg; 
		this.logElem.appendChild(textnode);    
		this.logElem.scrollTop = this.logElem.scrollHeight;
		return msg;		
	}
	
	error(msg: string): string {
		var textnode = document.createElement("p"); 
		textnode.className = "consoleText";
		textnode.style.color = "red";
		textnode.innerHTML = msg; 
		this.logElem.appendChild(textnode);    
		this.logElem.scrollTop = this.logElem.scrollHeight; 
		return msg;
	}
}
