import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ProjectDataService {

	private projectPathNotificator: BehaviorSubject<string>;
	public projectPath: string = "none";
	
	private projectNameNotificator: BehaviorSubject<string>;
	public projectName: string = "none";
	
	private projectDescriptionNotificator: BehaviorSubject<string>;
	public projectDescription: string = "";
	
	private cloudPlatfromAppIDNotificator: BehaviorSubject<string>;
	public cloudPlatfromAppID: string = "ApiTestBot";
	
	private cloudPlatfromLanguageNotificator: BehaviorSubject<string>;
	public cloudPlatfromLanguage: string = "en-en";
	
	private ocpApimSubscriptionNotificator: BehaviorSubject<string>;
	public ocp_apim_subscription_key:string = "b0b68a32871846c587fed8e202b9020c"
	
	private dialogManagementSecretNotificator: BehaviorSubject<string>;
	public dialogManagementSecret:string = "KEVSSGrBAbM.aHdn6XIpOwJ2XbdOujgDqLZ-9ndnQkZNuiUmZ9Vf_ro";
	
	private dialogManagementTokenNotificator: BehaviorSubject<string>;
	public dialogManagementToken:string = "";
	
	private dialogManagementConversationIDNotificator: BehaviorSubject<string>;
	public dialogManagementConversationID:string = "";
	
	private dialogManagementBotIDNotificator: BehaviorSubject<string>;
	public dialogManagementBotID:string = "prodbotcompris637944618";
	
	private sttModelsNotificator: BehaviorSubject<any>;
	public sttModels:any = [];
	
	private selectedSttModelsNotificator: BehaviorSubject<any>;
	public selectedSttModels:any = [];
	  
	private ttsModelsNotificator: BehaviorSubject<string>;
	public ttsModels:any = [];
	
	private installPrivacyDrivenSpeechTransformationNotificator : BehaviorSubject<boolean>;
	public installPrivacyDrivenSpeechTransformation:boolean = true;
	
	private installPrivacyDrivenTextTransformationNotificator : BehaviorSubject<boolean>;
	public installPrivacyDrivenTextTransformation:boolean = true;
	
	private installPersonalizedLearningNotificator : BehaviorSubject<boolean>;
	public installPersonalizedLearning:boolean = true;
	
	private installCloudPlatformNotificator : BehaviorSubject<boolean>;
	public installCloudPlatform:boolean = true;
	
	private installTtsNotificator : BehaviorSubject<boolean>;
	public installTts:boolean = true;
	
	private installSttNotificator : BehaviorSubject<boolean>;
	public installStt:boolean = true;
	
	private installMtNotificator : BehaviorSubject<boolean>;
	public installMt:boolean = true;
	
	private installSluNotificator : BehaviorSubject<boolean>;
	public installSlu:boolean = true;
	
	private installDmNotificator : BehaviorSubject<boolean>;
	public installDm:boolean = true;
	
	private personalizedServerURLNotificator : BehaviorSubject<string>;
	public personalizedServerURL:string = ""; //"https://94.130.225.120:8443/";
	
	constructor() {
		this.projectPathNotificator = new BehaviorSubject(this.projectPath);
		this.projectNameNotificator = new BehaviorSubject(this.projectName);
		this.projectDescriptionNotificator = new BehaviorSubject(this.projectDescription);
		this.cloudPlatfromAppIDNotificator = new BehaviorSubject(this.cloudPlatfromAppID);
		this.cloudPlatfromLanguageNotificator = new BehaviorSubject(this.cloudPlatfromLanguage);
		this.ocpApimSubscriptionNotificator = new BehaviorSubject(this.ocp_apim_subscription_key);
		this.dialogManagementSecretNotificator = new BehaviorSubject(this.dialogManagementSecret);
		this.dialogManagementTokenNotificator = new BehaviorSubject(this.dialogManagementToken);
		this.dialogManagementConversationIDNotificator = new BehaviorSubject(this.dialogManagementConversationID);
		this.dialogManagementBotIDNotificator = new BehaviorSubject(this.dialogManagementBotID);
		this.sttModelsNotificator = new BehaviorSubject(this.sttModels);
		this.selectedSttModelsNotificator = new BehaviorSubject(this.selectedSttModels);
		this.ttsModelsNotificator = new BehaviorSubject(this.ttsModels);
		this.installPrivacyDrivenSpeechTransformationNotificator = new BehaviorSubject(this.installPrivacyDrivenSpeechTransformation)
		this.installPrivacyDrivenTextTransformationNotificator = new BehaviorSubject(this.installPrivacyDrivenTextTransformation)
		this.installCloudPlatformNotificator = new BehaviorSubject(this.installCloudPlatform);
		this.installPersonalizedLearningNotificator = new BehaviorSubject(this.installPersonalizedLearning);
		this.installTtsNotificator = new BehaviorSubject(this.installTts);
		this.installSttNotificator = new BehaviorSubject(this.installStt);
		this.installMtNotificator = new BehaviorSubject(this.installMt);
		this.installSluNotificator = new BehaviorSubject(this.installSlu);
		this.installDmNotificator = new BehaviorSubject(this.installDm);
		this.personalizedServerURLNotificator = new BehaviorSubject(this.personalizedServerURL);
	}
	
	getProjectPath(): Observable<string> {
		return this.projectPathNotificator.asObservable();
	}
	
	setProjectPath(projectPath: string) {
		this.projectPath = projectPath;
		this.projectPathNotificator.next(projectPath);
	}
	
	getProjectName(): Observable<string> {
		return this.projectNameNotificator.asObservable();
	}
	
	setProjectDescription(projectDescription: string) {
		this.projectDescription = projectDescription;
		this.projectDescriptionNotificator.next(projectDescription);
	}
	
	getProjectDescription(): Observable<string> {
		return this.projectDescriptionNotificator.asObservable();
	}
	
	setProjectName(projectName: string) {
		this.projectName = projectName;
		this.projectNameNotificator.next(projectName);
	}
	
	getCloudPlatfromAppID(): Observable<string> {
		return this.cloudPlatfromAppIDNotificator.asObservable();
	}
	
	setCloudPlatfromAppID(cloudPlatfromAppID: string) {
		this.cloudPlatfromAppID = cloudPlatfromAppID;
		this.cloudPlatfromAppIDNotificator.next(cloudPlatfromAppID);
	}
	
	getCloudPlatfromLanguage(): Observable<string> {
		return this.cloudPlatfromLanguageNotificator.asObservable();
	}
	
	setCloudPlatfromLanguage(cloudPlatfromLanguage: string) {
		this.cloudPlatfromLanguage = cloudPlatfromLanguage;
		this.cloudPlatfromLanguageNotificator.next(cloudPlatfromLanguage);
	}
	
	getOcpApimSubscriptionKey(): Observable<string> {
		return this.ocpApimSubscriptionNotificator.asObservable();
	}
	
	setOcpApimSubscriptionKey(ocp_apim_subscription_key: string) {
		this.ocp_apim_subscription_key = ocp_apim_subscription_key;
		this.ocpApimSubscriptionNotificator.next(ocp_apim_subscription_key);
	}
	
	getDialogManagementSecret(): Observable<string> {
		return this.dialogManagementSecretNotificator.asObservable();
	}
	
	setDialogManagementSecret(dialogManagementSecret: string) {
		this.dialogManagementSecret = dialogManagementSecret;
		this.dialogManagementSecretNotificator.next(dialogManagementSecret);
	}
	
	getDialogManagementToken(): Observable<string> {
		return this.dialogManagementTokenNotificator.asObservable();
	}
	
	setDialogManagementToken(dialogManagementToken: string) {
		this.dialogManagementToken = dialogManagementToken;
		this.dialogManagementTokenNotificator.next(dialogManagementToken);
	}
	
	getDialogManagementConversationID(): Observable<string> {
		return this.dialogManagementConversationIDNotificator.asObservable();
	}
	
	setDialogManagementConversationID(dialogManagementConversationID: string) {
		this.dialogManagementConversationID = dialogManagementConversationID;
		this.dialogManagementConversationIDNotificator.next(dialogManagementConversationID);
	}
	
	getDialogManagementBotID(): Observable<string> {
		return this.dialogManagementBotIDNotificator.asObservable();
	}
	
	setDialogManagementBotID(dialogManagementBotID: string) {
		this.dialogManagementBotID = dialogManagementBotID;
		this.dialogManagementBotIDNotificator.next(dialogManagementBotID);
	}
	
	getSttModels(): Observable<any> {
		return this.sttModelsNotificator.asObservable();
	}
	
	setSttModels(sttModels: any) {
		this.sttModels = sttModels;
		this.sttModelsNotificator.next(sttModels);
	}
	
	getSelectedSttModels(): Observable<any> {
		return this.selectedSttModelsNotificator.asObservable();
	}
	
	setSelectedSttModels(selectedSttModels: any) { 
		this.selectedSttModels = selectedSttModels;
		this.selectedSttModelsNotificator.next(selectedSttModels);
	}
	
	getTtsModels(): Observable<any> {
		return this.ttsModelsNotificator.asObservable();
	}
	
	setTtsModels(ttsModels: any) {
		this.ttsModels = ttsModels;
		this.ttsModelsNotificator.next(ttsModels);
	}
	
	getInstallPrivacyDrivenSpeechTransformation() {
		return this.installPrivacyDrivenSpeechTransformationNotificator.asObservable();
	}
	
	setInstallPrivacyDrivenSpeechTransformation(installPrivacyDrivenSpeechTransformation: boolean) {
		this.installPrivacyDrivenSpeechTransformation = installPrivacyDrivenSpeechTransformation;
		this.installPrivacyDrivenSpeechTransformationNotificator.next(installPrivacyDrivenSpeechTransformation);
	}
	
	getInstallPrivacyDrivenTextTransformation() {
		return this.installPrivacyDrivenTextTransformationNotificator.asObservable();
	}
	
	setInstallPrivacyDrivenTextTransformation(installPrivacyDrivenTextTransformation: boolean) {
		this.installPrivacyDrivenTextTransformation = installPrivacyDrivenTextTransformation;
		this.installPrivacyDrivenTextTransformationNotificator.next(installPrivacyDrivenTextTransformation);
	}
	
	getInstallPersonalizedLearning() {
		return this.installPersonalizedLearningNotificator.asObservable();
	}
	
	setInstallPersonalizedLearning(installPersonalizedLearning: boolean) {
		this.installPersonalizedLearning = installPersonalizedLearning;
		this.installPersonalizedLearningNotificator.next(installPersonalizedLearning);
	}
	
	getInstallCloudPlatform() {
		return this.installCloudPlatformNotificator.asObservable();
	}
	
	setInstallCloudPlatform(installCloudPlatform: boolean) {
		this.installCloudPlatform = installCloudPlatform;
		this.installCloudPlatformNotificator.next(installCloudPlatform);
	}
	
	getInstallStt() {
		return this.installSttNotificator.asObservable();
	}
	
	setInstallStt(installStt: boolean) {
		this.installStt = installStt;
		this.installSttNotificator.next(installStt);
	}
	
	getInstallTts() {
		return this.installTtsNotificator.asObservable();
	}
	
	setInstallTts(installTts: boolean) {
		this.installTts = installTts;
		this.installTtsNotificator.next(installTts);
	}
	
	getInstallMt() {
		return this.installMtNotificator.asObservable();
	}
	
	setInstallMt(installMt: boolean) {
		this.installMt = installMt;
		this.installMtNotificator.next(installMt);
	}
	
	getInstallSlu() {
		return this.installSluNotificator.asObservable();
	}
	
	setInstallSlu(installSlu: boolean) {
		this.installSlu = installSlu;
		this.installSluNotificator.next(installSlu);
	}
	
	getInstallDm() {
		return this.installDmNotificator.asObservable();
	}
	
	setInstallDm(installDm: boolean) {
		this.installDm = installDm;
		this.installDmNotificator.next(installDm);
	}
	
	getPersonalizedServerURL() {
		return this.personalizedServerURLNotificator.asObservable();
	}
	
	setPersonalizedServerURL(personalizedServerURL: string) {	
		this.personalizedServerURL = personalizedServerURL;
		this.personalizedServerURLNotificator.next(personalizedServerURL);
	}
	

}
