import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ProjectDataService, ElectronService } from '../core/services';

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-dm',
  templateUrl: './dm.component.html',
  styleUrls: ['./dm.component.scss']
})
export class DmComponent implements OnInit {

  	constructor(private router: Router, 
		public projectDataService: ProjectDataService,
		public electronService: ElectronService,
		public httpClient: HttpClient) {
		console.log(this.router.url)
	}

	ngOnInit(): void {
		this.openConversation()
	}
	
	openConversation() {
		let env = this;
		
		let headers = new HttpHeaders();
		headers = headers.set("Authorization", "Bearer "+env.projectDataService.dialogManagementSecret); 
		headers = headers.set("Content-Type", "application/json");
		
		this.httpClient.post("https://directline.botframework.com/v3/directline/conversations", {}, {headers: headers}).subscribe((data: any)=>{
			console.log("GOT CONVERSATION", data)
			env.projectDataService.setDialogManagementToken(data.token)
			env.projectDataService.setDialogManagementConversationID(data.conversationId)  
			env.getAnswer();
		})  
	}
	
	retrieveConversation() {
		let env = this;
		
		let headers = new HttpHeaders();
		headers = headers.set("Authorization", "Bearer "+env.projectDataService.dialogManagementSecret); 
		headers = headers.set("Content-Type", "application/json");
		
		this.httpClient.get("https://directline.botframework.com/v3/directline/conversations/1iZRkWHHQO11V35cBPmzHL-h", {headers: headers}).subscribe((data: any)=>{
			console.log("GOT CONVERSATION", data)
			env.projectDataService.setDialogManagementToken(data.token)
			env.projectDataService.setDialogManagementConversationID(data.conversationId)  
			env.sendMessage();
		})  
	}
	
	sendMessage()  {
		 
		let env = this;
		
		let headers = new HttpHeaders();
		headers = headers.set("Authorization", "Bearer "+env.projectDataService.dialogManagementToken); 
		headers = headers.set("Content-Type", "application/json");

		let msgElem = (<HTMLInputElement> document.getElementById("dm_message_input"));
		let msg = msgElem.value;
		msgElem.value = "";

		let body = {
			"type": "message",
			"from": {
				"id": "123",
				"name": "Gerrit"
			},
			"text": msg
		}

		
		this.httpClient.post("https://directline.botframework.com/v3/directline/conversations/"+
			env.projectDataService.dialogManagementConversationID+"/activities", body, {headers: headers}).subscribe((data: any)=>{
			console.log("GOT MESSAGE", data)
			env.getAnswer();
		})  
	}
	
	
	getAnswer() {
		let env = this;
		
		var dmConversationTable = <HTMLTableElement>document.getElementById("dm_conversation_table");
		
		
		let headers = new HttpHeaders();
		headers = headers.set("Authorization", "Bearer "+env.projectDataService.dialogManagementSecret); 
		headers = headers.set("Content-Type", "application/json");
		
		this.httpClient.get("https://directline.botframework.com/v3/directline/conversations/1iZRkWHHQO11V35cBPmzHL-h/activities", {headers: headers}).subscribe((data: any)=>{
			console.log("GOT ANSWER", data) 
			
			for(var message of data.activities) {
				if(message.type === "message" && 
					(message.text !== "" || message.attachments[0].content.text !== "") &&
					document.getElementById(message.id) == undefined) {					
						let tr = document.createElement("tr");
						tr.id = message.id
						let th_user = document.createElement("th");
						let th_bot = document.createElement("th");
						
						tr.appendChild(th_user);
						tr.appendChild(th_bot);
						
						console.log(message.from.name , env.projectDataService.dialogManagementBotID, message.from.name === env.projectDataService.dialogManagementBotID)
						
						let p = document.createElement("p");
						p.innerHTML = message.text || message.attachments[0].content.text
						if(message.from.name === env.projectDataService.dialogManagementBotID) {
							th_bot.appendChild(p)
						} else {
							th_user.appendChild(p)
						}
						
						dmConversationTable.appendChild(tr)  
				}
			}
			dmConversationTable.scrollTop = dmConversationTable.scrollHeight
		})  
	}
	
	/*test() {
		let env = this;
		
		let headers = new HttpHeaders();
		headers = headers.set("Content-Type", "application/x-www-form-urlencoded");
		
		let httpParams = new HttpParams()
			.append("grant_type", "client_credentials")
			.append("client_id", "e465533f-b24a-4254-ae5d-c0721e04a115")
			.append("client_secret", "pG=qocuBMckT1:WnPFGz8JbVC@PY[z22")
			.append("scope","https://api.botframework.com/.default").toString();
			
		this.httpClient.post("https://login.microsoftonline.com/botframework.com/oauth2/v2.0/token", httpParams, {headers: headers}).subscribe((data: any)=>{
			console.log(data)
			env.sendMessage(data.access_token);
		})  
	}
	
	sendMessage(access_token) { 
		
		let headers = new HttpHeaders();
		headers = headers.set("Authorization", "Bearer "+access_token);
		headers = headers.set("Content-Type", "application/json");
		
		let body = {
			"bot": {
				"id": "65",
				"name": "prodbotcompris637944618"
			},
			"isGroup": false,
			"members": [
				{
					"id": "1234abcd",
					"name": "recipient's name"
				}
			],
			"topicName": "News Alert"
		}
		
		this.httpClient.post("https://prodbotcompris637944618.azurewebsites.net/api/messages/v3/conversations", body, {headers: headers}).subscribe((data: any)=>{
			console.log(data)
		}) 
	}*/

}
