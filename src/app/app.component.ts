import { Component, AfterViewInit } from '@angular/core';
import { ElectronService, ProjectDataService, LoggingService } from './core/services';
import { TranslateService } from '@ngx-translate/core';
import { AppConfig } from '../environments/environment';
import { Router } from '@angular/router';
import { shell } from 'electron'; 
import * as fse from 'fs-extra';

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
	
	logElem;
	
	log() {
		console.log(this.router.url)
	}
	
	constructor(
		public electronService: ElectronService,
		public projectDataService: ProjectDataService,
		public loggingService: LoggingService,
		private translate: TranslateService,
		public router: Router, 
		public httpClient: HttpClient
	) {
		translate.setDefaultLang('en');
		console.log('AppConfig', AppConfig);

		if (electronService.isElectron) {
			console.log(process.env);
			console.log('Mode electron');
			console.log('Electron ipcRenderer', electronService.ipcRenderer);
			console.log('NodeJS childProcess', electronService.childProcess);
		} else {
			console.log('Mode web');  
		}
	}
	
	navigateDocu() {		
		shell.openExternal("https://gitlab.inria.fr/comprise/comprise_app_wizard/-/blob/master/README.md");
	}
	navigateDocu1() {		
		shell.openExternal("https://gitlab.inria.fr/comprise/comprise_client_library/-/blob/master/README.md");
	}
	navigateDocu2() {		
		shell.openExternal("https://gitlab.inria.fr/comprise/comprise-personal-server/-/blob/master/README.md");
	}
	navigateDocu3() {		
		shell.openExternal("https://gitlab.inria.fr/comprise/comprise_sample_app");
	}
	
	
	
	
	navigateSLU() {
		shell.openExternal("https://botdashboard.tilde.ai/Intents");
	}
	
	navigateDM() {
		shell.openExternal("https://botdashboard.tilde.ai/Model");
	}
	
	navigateSLG() {
		shell.openExternal("https://botdashboard.tilde.ai/Lang");
	}
	
	ngAfterViewInit() {  
		this.loggingService.logElem = document.getElementById("logframe");
		this.loggingService.info("SDK loaded."); 
		let env = this;
	} 
  
	installDependencies() {
		let env = this;
		 
		var componentsToAdd = "@angular-devkit/build-optimizer @angular-devkit/build-angular node-properties-parser "  +
			"@angular/animations @angular/cdk @angular/common @angular/core @angular/forms " + 
			"@angular/material @angular/platform-browser @angular/platform-browser-dynamic @angular/router @angular/service-worker" +
			" @angular-devkit/schematics @angular/cli @angular/compiler @angular/compiler-cli @angular/language-service";
		
		if(this.projectDataService.installPrivacyDrivenSpeechTransformation) {
			componentsToAdd += " comprise_privacy_driven_speech_transformation@latest";
		}
		
		if(this.projectDataService.installPrivacyDrivenTextTransformation) {
			componentsToAdd += " comprise_privacy_driven_text_transformation@latest";
		}
		
		if(this.projectDataService.installPersonalizedLearning) {
			componentsToAdd += " comprise_personalized_learning@latest";
		}
		
		if(this.projectDataService.installCloudPlatform) {
			componentsToAdd += " comprise_cloud_platform_api@latest";
		}
		
		if(this.projectDataService.installMt) {
			componentsToAdd += " comprise_machine_translation@latest";
		}
		if(this.projectDataService.installDm) {
			componentsToAdd += " comprise_natural_language_generation@latest";
		}
		
		if(this.projectDataService.installSlu) {
			componentsToAdd += " comprise_natural_language_understanding@latest";
		} 
		
		if(this.projectDataService.installStt) {
			componentsToAdd += " comprise_speech_to_text@latest";
		}
		
		if(this.projectDataService.installTts) {
			componentsToAdd += " comprise_text_to_speech@latest";
		}
		
		if(this.projectDataService.personalizedServerURL.charAt(this.projectDataService.personalizedServerURL.length-1) === "/") {
			this.projectDataService.personalizedServerURL = this.projectDataService.personalizedServerURL.substring(0, this.projectDataService.personalizedServerURL.length-1);
		}
		
		var packageFile = JSON.parse(this.electronService.fs.readFileSync(this.projectDataService.projectPath+"/package.json", "utf8"));
				
		packageFile.personalizedServerURL =  this.projectDataService.personalizedServerURL;
		packageFile.scripts.update_cordova_plugins = "(ionic cordova plugin remove cordova-plugin-comprise-speech-to-text --save || true) && ionic cordova plugin add cordova-plugin-comprise-speech-to-text --save"; 
		
		packageFile.scripts.generate_android = "ionic cordova platform remove android --no-interactive --confirm && ionic cordova platform add android@8 --no-interactive --confirm && npm run update_cordova_plugins && cp -rf ./models ./platforms/android/ && cp -rf ./tmp/build/raw ./platforms/android/app/src/main/res/ && cp -f ./tmp/build/gradle.properties ./platforms/android/ && cp -f ./tmp/build/project.properties ./platforms/android/ && cp -f ./tmp/build/app/AndroidManifest.xml  ./platforms/android/app/src/main/ && cp -f ./tmp/build/cordova/AndroidManifest.xml ./platforms/android/CordovaLib/ && ionic cordova build android --no-interactive --confirm"; 
		
		var supportedLanguages = [];
		
		try {
			this.electronService.childProcess.execSync('cp -r ' + this.electronService.remote.app.getAppPath() + '/models' + ' ' + this.projectDataService.projectPath)
			this.electronService.childProcess.execSync('cp -r ' + this.electronService.remote.app.getAppPath() + '/tmp' + ' ' + this.projectDataService.projectPath)
			this.electronService.childProcess.execSync('cp ' + this.electronService.remote.app.getAppPath() + '/config.xml' + ' ' + this.projectDataService.projectPath)

		} catch(err) {
			console.error(err);
		}

		for(var sttModel of this.projectDataService.selectedSttModels) { 
			if(sttModel.lang === "en") supportedLanguages.push("en-US");
			if(sttModel.lang === "de") supportedLanguages.push("de-DE"); 
			if(sttModel.lang === "pt") supportedLanguages.push("pt-PT");
			if(sttModel.lang === "fr") supportedLanguages.push("fr-FR");
			if(sttModel.lang === "lv") supportedLanguages.push("lv-LV");
			if(sttModel.lang === "lt") supportedLanguages.push("lt-LT");
		}  
		
		packageFile.supportedLanguages =  supportedLanguages.toString();
		
		//alert(packageFile.personalizedServerURL)
		
		this.electronService.fs.writeFileSync(this.projectDataService.projectPath+"/package.json",JSON.stringify(packageFile))

		let headers = new HttpHeaders(); 
		let body = {
			"chosen_stt_models": this.projectDataService.selectedSttModels
		};
		
		var installCommand = "cd "+this.projectDataService.projectPath+" && npm uninstall "+ componentsToAdd +" && npm i --save --force "+componentsToAdd; 
		
		this.loggingService.info("Downloading Models to Personal Server in the background...");
		
		this.httpClient.post(this.projectDataService.personalizedServerURL+"/set_stt_models", body, {headers: headers}).subscribe((data: any)=>{
			this.loggingService.info("Installing COMPRISE Components to project '"+packageFile.name+"'...");
			setTimeout(() => {
				this.loggingService.info(installCommand);
	 
				var child = this.electronService.childProcess.spawn(installCommand,[], {
					shell: true 
				});
				
				child.stdout.on('data', (data) => {
					this.loggingService.angularLog(data)
				});
				
				child.stderr.on('data', (error) => { 
					this.loggingService.angularLog(error)
				});
				
				child.on('close', function(code) {
					env.buildApp();
				}); 
			},3000);
		})  


		
	}
  
	buildApp() { 
 	
		this.loggingService.info("Build process was initialized. Processing...")		
				
		var child = this.electronService.childProcess.spawn("cd "+this.projectDataService.projectPath+" && npm run generate_android",[], {
			shell: true 
		});
		
		child.stdout.on('data', (data) => {
			this.loggingService.angularLog(data)
		});
		
		child.stderr.on('data', (error) => { 
			this.loggingService.angularLog(error)		
		});
		
		child.on('close', function(code) {
			this.loggingService.success("Your build ended successfully. You can now launch your app.")		
		}); 
		
	}
}
