import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SttComponent } from './stt.component';

const routes: Routes = [
	{
		path: '',
		component: SttComponent
	}
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SttRoutingModule { }
