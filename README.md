<img src="./src/assets/img/comprise_logo.png" width="200" height="200">

![Maintained][maintained-badge]
[![Travis Build Status][build-badge]][build]
[![Make a pull request][prs-badge]][prs]
[![License](http://img.shields.io/badge/Licence-MIT-brightgreen.svg)](LICENSE.md)

[![Tweet][twitter-badge]][twitter]

# Dependencies

[![Angular Logo](https://www.vectorlogo.zone/logos/angular/angular-icon.svg)](https://angular.io/) [![Electron Logo](https://www.vectorlogo.zone/logos/electronjs/electronjs-icon.svg)](https://electronjs.org/)

Currently runs with:

- Angular v9.1.4
- Electron v8.2.5
- Electron Builder v22.6.0

Needed for execution:

- Node 10.13 or later. We recommend latest LTS Version 12.x.
- Git
- See [package.json](./package.json) for the full list.

# Installation
- The COMPRISE App Wizard should be installed after the [COMPRISE Personal Server](https://gitlab.inria.fr/comprise/comprise-personal-server) and before the [COMPRISE Client Library](https://gitlab.inria.fr/comprise/comprise_client_libraries).
An installation & usage overview video can be found on YouTube [here](https://www.youtube.com/watch?v=ZqNcCc-tdBk).

Install the latest LTS version of [NodeJS](https://nodejs.org/en/download/) and [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) on your system, following the advice on the corresponding URLs.

Clone this repository locally:

``` bash
git clone https://gitlab.inria.fr/comprise/comprise_app_wizard.git
```

Also, install dependencies of COMPRISE App Wizard with npm (part of installed NodeJS) and `@angular/cli` in npm global context.

``` bash
npm install -g @angular/cli && npm install
```

## Start of application
 
- **in a terminal window** 

``` bash
npm start
```

Voila! You can use your Angular + Electron app in a local development environment with hot reload !

The application code is managed by `main.ts`. In this sample, the app runs with a simple Angular App (http://localhost:4200) and an Electron window.
The Angular component contains an example of Electron and NodeJS native lib import.
You can disable "Developer Tools" by commenting `win.webContents.openDevTools();` in `main.ts`.

## Included Commands

|Command|Description|
|--|--|
|`npm run ng:serve:web`| Execute the app in the browser |
|`npm run build`| Build the app. Your built files are in the /dist folder. |
|`npm run build:prod`| Build the app with Angular aot. Your built files are in the /dist folder. |
|`npm run electron:local`| Builds your application and start electron
|`npm run electron:linux`| Builds your application and creates an app consumable on linux system |
|`npm run electron:windows`| On a Windows OS, builds your application and creates an app consumable in windows 32/64 bit systems |
|`npm run electron:mac`|  On a MAC OS, builds your application and generates a `.app` file of your application that can be run on Mac |

**Your application is optimised. Only /dist folder and node dependencies are included in the executable.**



# How to use 

When the COMPRISE App Wizard has been started, you will see the screen below. You will start within the **Overview-Frame (a)**. The steps below will guide you through other components.

If you accidentally close this documentation, just click **Documentation (b)** to get it back. 

Sometimes, in case of installation processes or errors, check the **Console (c)** for more information.

<img src="./doc/step1.png">

## Step 1 - COMPRISE Project Initialization


The first step for the developer will be to choose the project which needs to be enriched by the COMPRISE Client Libraries functionality. Click on "Load COMPRISE project" in **Step 1**. 

<img src="./doc/step2.png" height="200">

A file chooser will open to let you choose a project you want to attach COMPRISE Client Libraries to. Currently, we support Angular projects within Ionic applications.
Enter the folder of the project of your choice. Here, we choose "comprise_sample_app" as an example. Of course, you should do this with your own project.
Each Angular project has a **"package.json"**-file. Click it to complete project selection.

## Step 2 - COMPRISE Client Libraries Sub-Component and Personalized Server Configuration

If you have chosen your project to work with, you can assign the URL generated within the [Personalized Server](https://gitlab.inria.fr/comprise/comprise_personalized_server).
It will be assigned to the **"package.json"**-file of your project later and be used by the Client Libraries to contact to Server.

You also can choose which (sub-) modules should be part of the COMPRISE Client Libraries, or in other words: Which functions
will be present when your Smartphone app is running later on. By default, every component is enabled.

Currently supported are:

**Operation Branch:**

- Speech-To-Text
- Machine Translation
- Spoken Language Understanding
- Dialog Management
- Spoken Language Generation
- Text-To-Speech

**Training Branch:**

- Privacy-Driven Speech Transformation
- Privacy-Driven Text Transformation

**Still open:**

- Cloud Platform
- Personalized Learning

The integration is still in progress, so stay tuned for new functionality!

## Step 3 - Cloud Platform

When you loaded your project in **Step 1**, this automatically caused the creation of a application container within the Cloud Platform.
The platform allows users to upload, store and manage data and labels and train or access large-scale userindependent models trained on these data. The platform functionality includes secure cloud-based data and model storage, scalable and dynamic cloud-based high-performance computing, APIs for continuous data upload and occasional model download, and general platform features (user interface, authentication, usage analytics, etc.) and procedures for data labeling and curation.

More information to be found on [Deliverable D5.2](https://www.compriseh2020.eu/files/2019/11/D5.2.pdf).

## Step 4 - Spoken Language Understanding

In this step, you will train how your input is interpreted by the system in which intent is identified. 
Find all documentation needed within the [Training PDF](./doc/Tilde.AI Training.pdf) or the [Training Video](https://www.youtube.com/watch?v=fVciLmr1VDI&feature=youtu.be).

## Step 5 - Dialog Management

After the SLU-Training, you need to define on how the system will react to the detected intent. The way to proceed is the same then in **Step 4**.
 
## Step 6 - Spoken Language Generation

Based on the the result of the Dialog Management, you will need the system to tell which answer shall be generated. The way to proceed is the same then in **Step 4**.
 
## Step 7 - Attach / Update the COMPRISE Client Libraries

By clicking **Install COMPRISE Client Libraries To Device**, all of the components you chose in **Step 2** will be part COMPRISE Client Libraries and installed as part of the dependencies of your application.
In your console, you will see the ongoing progress of installation of the COMPRISE Client Libraries components and a test build, to ensure everything went well.

It should look something like below. You also can use the App Wizard for updating packages already present. Then, the output might be a little bigger.

<img src="./doc/step3.png">

The project you choose in **Step 1**, or better the dependencies in **package.json**, now will be uploaded and the components are installed / updated in the node_modules-folder of your project.

<img src="./doc/step4.png">

## Step 8 - Start coding with the COMPRISE Client Libraries!

You can start using the provided functionality of the Client Libraries!

Open your personal project in your preferred IDE and have a look at the COMPRISE Client Libraries functionality to be used within your app:

https://gitlab.inria.fr/comprise/comprise_client_libraries

 

[build-badge]: https://travis-ci.org/maximegris/angular-electron.svg?branch=master&style=style=flat-square
[build]: https://travis-ci.org/maximegris/angular-electron
[license-badge]: https://img.shields.io/badge/license-Apache2-blue.svg?style=style=flat-square
[license]: https://github.com/maximegris/angular-electron/blob/master/LICENSE.md
[prs-badge]: https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square
[prs]: http://makeapullrequest.com
[github-watch-badge]: https://img.shields.io/github/watchers/maximegris/angular-electron.svg?style=social
[github-watch]: https://github.com/maximegris/angular-electron/watchers
[github-star-badge]: https://img.shields.io/github/stars/maximegris/angular-electron.svg?style=social
[github-star]: https://github.com/maximegris/angular-electron/stargazers
[twitter]: https://twitter.com/intent/tweet?text=Check%20out%20COMPRISE%20SDK%20Developer%20UI!%20https://gitlab.inria.fr/comprise/comprise_sdk_developer_ui/-/tree/electron/
[twitter-badge]: https://img.shields.io/twitter/url/https/github.com/maximegris/angular-electron.svg?style=social
[maintained-badge]: https://img.shields.io/badge/maintained-yes-brightgreen
